"""Utility functions for working with paths."""

import os
from typing import Tuple, Sequence, Optional


def path_relative_to_script(script_path: str, relative_path: str) -> str:
    """
    Calculate a path relative to the script.

    Parameters
    ----------
    script_path: str
        The path of the calling script - pass in __file__ in most cases.
    relative_path: str
        The path of the desired file or directory relative to the calling script.

    Returns
    -------
    str: A path that points to the intended file or directory.
    """
    return os.path.join(os.path.dirname(os.path.realpath(script_path)), relative_path)


def split_path(path: str) -> Tuple[Sequence[str], Optional[str]]:
    """
    Split a path into drive, directory a file names.

    Splits a path string into a list of directory names and
    a file name. If the path does not exist, the last part
    is assumed to be a directory name if it has a trailing
    slash or a file name otherwise.

    If the path includes a drive letter, that will be the first
    entry in the directory list.

    If the path is absolute, the initial slash will have its
    own entry at the beginning of the directory list - after
    the drive letter if there is one.

    Parameters
    ----------
    aPath: str
        Path to split. Can be relative or absolute.

    Returns
    -------
    (dirs, file):
        dirs = list of strings giving the directory parts.
        file = file name, if present. None otherwise.
    """
    dirs = []
    file_name = None

    # Break off the drive letter if present.
    drive: Optional[str]
    head: str
    drive, head = os.path.splitdrive(path)
    if len(drive or "") < 1:
        drive = None

    is_absolute = False

    # Break the string into parts in right to left order.
    while len(head or "") > 0:
        head, tail = os.path.split(head)
        if len(tail) > 0:
            dirs.append(tail)
        elif head == os.path.sep:
            is_absolute = True
            break

    if len(dirs) < 1:
        return ([], None)

    # Determine whether it's a directory or a file.
    if not (os.path.isdir(path) or path.endswith("/") or path.endswith("\\")):
        # If it looks like a filename, take it off the directory list.
        file_name = dirs[0]
        dirs = dirs[1:]
        if len(file_name) < 1:
            file_name = None

    # If it's an absolute path, add an initial slash.
    if is_absolute:
        dirs.append(os.path.sep)

    # If there is a drive letter, put it at the start.
    if drive is not None:
        dirs.append(drive)

    # Put the remaining directory parts in order.
    dirs.reverse()

    return (dirs, file_name)

